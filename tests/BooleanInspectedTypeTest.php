<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\BooleanInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * BooleanInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\BooleanInspectedType
 *
 * @internal
 *
 * @small
 */
class BooleanInspectedTypeTest extends TestCase
{
	
	/**
	 * @var BooleanInspectedType
	 */
	protected BooleanInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('boolean', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new BooleanInspectedType()));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('boolean', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new BooleanInspectedType();
	}
	
}
