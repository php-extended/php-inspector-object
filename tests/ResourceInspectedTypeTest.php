<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\ResourceInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * ResourceInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\ResourceInspectedType
 *
 * @internal
 *
 * @small
 */
class ResourceInspectedTypeTest extends TestCase
{
	
	/**
	 * @var ResourceInspectedType
	 */
	protected ResourceInspectedType $_type;
	
	/**
	 * @var resource
	 */
	protected $_res;
	
	public function testToString() : void
	{
		$this->assertEquals('resource(stream)', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new ResourceInspectedType($this->_res)));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('resource(stream)', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_res = \fopen(__FILE__, 'r');
		$this->_type = new ResourceInspectedType($this->_res);
	}
	
	protected function tearDown() : void
	{
		\fclose($this->_res);
	}
	
}
