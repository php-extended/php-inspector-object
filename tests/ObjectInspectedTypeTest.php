<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\ObjectInspectedType;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * ObjectInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\ObjectInspectedType
 *
 * @internal
 *
 * @small
 */
class ObjectInspectedTypeTest extends TestCase
{
	
	/**
	 * @var ObjectInspectedType
	 */
	protected ObjectInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('\\stdClass', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new ObjectInspectedType(new stdClass())));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('\\stdClass', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new ObjectInspectedType(new stdClass());
	}
	
}
