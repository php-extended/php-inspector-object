<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\ArrayInspectedType;
use PhpExtended\Inspector\BooleanInspectedType;
use PhpExtended\Inspector\FloatInspectedType;
use PhpExtended\Inspector\Inspector;
use PhpExtended\Inspector\IntegerInspectedType;
use PhpExtended\Inspector\NullInspectedType;
use PhpExtended\Inspector\ObjectInspectedType;
use PhpExtended\Inspector\ResourceInspectedType;
use PhpExtended\Inspector\StringInspectedType;
use PhpExtended\Inspector\UnknownInspectedType;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * InspectorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\Inspector
 *
 * @internal
 *
 * @small
 */
class InspectorTest extends TestCase
{
	
	/**
	 * @var Inspector
	 */
	protected Inspector $_inspector;
	
	/**
	 * @var resource
	 */
	protected $_openRes;
	
	/**
	 * @var resource
	 */
	protected $_closeRes;
	
	public function testToString() : void
	{
		$this->assertEquals(Inspector::class, $this->_inspector->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_inspector->equals(new Inspector()));
	}
	
	public function testInspectNull() : void
	{
		$this->assertEquals(new NullInspectedType(), $this->_inspector->inspect(null));
	}
	
	public function testInspectBoolean() : void
	{
		$this->assertEquals(new BooleanInspectedType(), $this->_inspector->inspect(true));
	}
	
	public function testInspectInteger() : void
	{
		$this->assertEquals(new IntegerInspectedType(1), $this->_inspector->inspect(1));
	}
	
	public function testInspectFloat() : void
	{
		$this->assertEquals(new FloatInspectedType(1.5), $this->_inspector->inspect(1.5));
	}
	
	public function testInspectString() : void
	{
		$this->assertEquals(new StringInspectedType('toto'), $this->_inspector->inspect('toto'));
	}
	
	public function testInspectArray() : void
	{
		$this->assertEquals(new ArrayInspectedType([new ArrayInspectedType([new BooleanInspectedType()])]), $this->_inspector->inspect([[true]]));
	}
	
	public function testInspectArray2() : void
	{
		$this->assertEquals(new ArrayInspectedType([new IntegerInspectedType(1), new FloatInspectedType(1.5)]), $this->_inspector->inspect([1, 2, 1.5, 2.5]));
	}
	
	public function testInspectMatrix() : void
	{
		$this->assertEquals(new ArrayInspectedType([new ArrayInspectedType([new IntegerInspectedType(1)])]), $this->_inspector->inspect([[1], [1]]));
	}
	
	public function testInspectNodepths() : void
	{
		$this->assertEquals(new ArrayInspectedType([]), $this->_inspector->inspect([], -1));
	}
	
	public function testInspectObject() : void
	{
		$this->assertEquals(new ObjectInspectedType(new stdClass()), $this->_inspector->inspect(new stdClass()));
	}
	
	public function testInspectResource() : void
	{
		$this->assertEquals(new ResourceInspectedType($this->_openRes), $this->_inspector->inspect($this->_openRes));
	}
	
	public function testInspectUnknown() : void
	{
		$this->assertEquals(new UnknownInspectedType(), $this->_inspector->inspect($this->_closeRes));
	}
	
	protected function setUp() : void
	{
		$this->_closeRes = \fopen(__FILE__, 'r');
		\fclose($this->_closeRes);
		$this->_openRes = \fopen(__FILE__, 'r');
		$this->_inspector = new Inspector();
	}
	
	protected function tearDown() : void
	{
		\fclose($this->_openRes);
	}
	
	
}
