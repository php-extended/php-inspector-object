<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\StringInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * StringInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\StringInspectedType
 *
 * @internal
 *
 * @small
 */
class StringInspectedTypeTest extends TestCase
{
	
	/**
	 * @var StringInspectedType
	 */
	protected StringInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('string', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new StringInspectedType('tata')));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('toto', $this->_type->getSample());
	}
	
	public function testBigSample() : void
	{
		$this->assertEquals('abcdefghijklmnopqrstuvwxyzA...', (new StringInspectedType('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'))->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new StringInspectedType('toto');
	}
	
}
