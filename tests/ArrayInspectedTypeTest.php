<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\ArrayInspectedType;
use PhpExtended\Inspector\BooleanInspectedType;
use PhpExtended\Inspector\FloatInspectedType;
use PhpExtended\Inspector\IntegerInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * ArrayInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\ArrayInspectedType
 *
 * @internal
 *
 * @small
 */
class ArrayInspectedTypeTest extends TestCase
{
	
	/**
	 * @var ArrayInspectedType
	 */
	protected ArrayInspectedType $_type;
	
	public function testEmptyString() : void
	{
		$this->assertEquals('[...]', (new ArrayInspectedType([]))->__toString());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('[integer, float]', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new ArrayInspectedType([
			new FloatInspectedType(2.5),
			new IntegerInspectedType(2),
		])));
	}
	
	public function testNotSameType() : void
	{
		$this->assertFalse($this->_type->equals(new BooleanInspectedType()));
	}
	
	public function testNotEnoughInner() : void
	{
		$this->assertFalse($this->_type->equals(new ArrayInspectedType([
			new BooleanInspectedType(),
		])));
	}
	
	public function testNotSameInner() : void
	{
		$this->assertFalse($this->_type->equals(new ArrayInspectedType([
			new IntegerInspectedType(1),
			new BooleanInspectedType(),
		])));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('[1, 1.5]', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new ArrayInspectedType([
			new IntegerInspectedType(1),
			new FloatInspectedType(1.5),
		]);
	}
	
}
