<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\NullInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * NullInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\NullInspectedType
 *
 * @internal
 *
 * @small
 */
class NullInspectedTypeTest extends TestCase
{
	
	/**
	 * @var NullInspectedType
	 */
	protected NullInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('null', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new NullInspectedType()));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('null', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new NullInspectedType();
	}
	
}
