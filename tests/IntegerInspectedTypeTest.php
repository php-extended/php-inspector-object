<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\IntegerInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * IntegerInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\IntegerInspectedType
 *
 * @internal
 *
 * @small
 */
class IntegerInspectedTypeTest extends TestCase
{
	
	/**
	 * @var IntegerInspectedType
	 */
	protected IntegerInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('integer', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new IntegerInspectedType(2)));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('1', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new IntegerInspectedType(1);
	}
	
}
