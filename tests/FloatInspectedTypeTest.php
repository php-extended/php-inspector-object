<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector\Test;

use PhpExtended\Inspector\FloatInspectedType;
use PHPUnit\Framework\TestCase;

/**
 * FloatInspectedTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Inspector\FloatInspectedType
 *
 * @internal
 *
 * @small
 */
class FloatInspectedTypeTest extends TestCase
{
	
	/**
	 * @var FloatInspectedType
	 */
	protected FloatInspectedType $_type;
	
	public function testToString() : void
	{
		$this->assertEquals('float', $this->_type->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_type->equals(new FloatInspectedType(2.0)));
	}
	
	public function testSample() : void
	{
		$this->assertEquals('1.5', $this->_type->getSample());
	}
	
	protected function setUp() : void
	{
		$this->_type = new FloatInspectedType(1.5);
	}
	
}
