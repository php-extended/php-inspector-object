# php-extended/php-inspector-object

An implementation of the php-inspector-interface library.

![coverage](https://gitlab.com/php-extended/php-inspector-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-inspector-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-inspector-object ^8`


## Basic Usage

In your code (for example, when trying to use a variable in an exception):

```php 

use PhpExtended\Inspect\Inspector;

if(!'<condition not satisfied with $myVar >')
{
	throw new Exception(strtr(
		'The given variable "myVar" is not an object, it\'s a {thing}.',
		['{thing}' => $inspector->inspect($myVar)]
	));
}

```

The expected results are the following :

```php

use PhpExtended\Inspect\Inspector;

$inspector = new Inspector();

echo $inspector->inspect(null);                 // echo 'null'
echo $inspector->inspect(false);                // echo 'boolean'
echo $inspector->inspect('myvar');              // echo 'string'
echo $inspector->inspect(1);                    // echo 'integer'
echo $inspector->inspect(1.5);                  // echo 'float'
echo $inspector->inspect(new stdClass());       // echo '\stdClass'
echo $inspector->inspect(fopen(__FILE__, 'r')); // echo 'resource(stream)'
echo $inspector->inspect([1]);                  // echo '[integer]'
echo $inspector->inspect([1.5]);                // echo '[float]'
echo $inspector->inspect([1, 1.5]);             // echo '[integer, float]'
echo $inspector->inspect([1, 2, 3]);            // echo '[integer]'
echo $inspector->inspect([new stdClass()]);     // echo '[\stdClass]'
echo $inspector->inspect([[1]]);                // echo '[[integer]]'

```


## License

MIT (See [license file](LICENSE)).
