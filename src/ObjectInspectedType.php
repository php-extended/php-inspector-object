<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * ObjectInspectedType class file.
 *
 * This class represents the fact that an object was inspected.
 *
 * @author Anastaszor
 */
class ObjectInspectedType implements InspectedTypeInterface
{
	
	/**
	 * The object to inspect.
	 *
	 * @var object
	 */
	protected object $_object;
	
	/**
	 * Builds a new ObjectInspectedType with the given object.
	 *
	 * @param object $object
	 */
	public function __construct($object)
	{
		$this->_object = $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getClassname();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self
			&& $this->getClassname() === $object->getClassname();
	}
	
	/**
	 * Gets the name of the class of the value object.
	 *
	 * @return string
	 */
	public function getClassname() : string
	{
		return '\\'.\get_class($this->_object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		return $this->getClassname();
	}
	
}
