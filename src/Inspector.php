<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * Inspector class file.
 *
 * This class makes statements about what is some variable. If you want to
 * inspect a variable, then do :
 *
 * ```php
 * 	// suppose $myUnknownVariable is instance of class Myclass
 * 	$inspector = new \PhpExtended\Inspect\Inspector($myUnknownVariable);
 *
 * 	echo $inspector; 	// will trigger __toString()
 * 						// and show something like "object(Myclass)"
 * ```
 *
 * @author Anastaszor
 */
class Inspector implements InspectorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return __CLASS__;
	}
	
	/**
	 * Effectively does the inspection of one-level recursivity. Recursivity
	 * is used only on arrays.
	 *
	 * @param null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object>> $variable
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $known
	 * @param integer $untilDepths the maximum depths to inspect
	 * @return InspectedTypeInterface
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function doInspectRecursive($variable, array &$known, int $untilDepths) : InspectedTypeInterface
	{
		if(null === $variable)
		{
			return new NullInspectedType();
		}
		
		if(\is_bool($variable))
		{
			return new BooleanInspectedType();
		}
		
		if(\is_int($variable))
		{
			return new IntegerInspectedType($variable);
		}
		
		if(\is_float($variable))
		{
			return new FloatInspectedType($variable);
		}
		
		if(\is_string($variable))
		{
			return new StringInspectedType($variable);
		}
		
		if(\is_array($variable))
		{
			return $this->doInspectRecursiveArray($variable, $known, $untilDepths);
		}
		
		if(\is_object($variable))
		{
			return new ObjectInspectedType($variable);
		}
		
		if(\is_resource($variable))
		{
			return new ResourceInspectedType($variable);
		}
		
		// returned for closed resources
		return new UnknownInspectedType();
	}
	
	/**
	 * Inspects the variable as array.
	 * 
	 * @param array<integer|string, null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object>> $variable
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $known
	 * @param int $untilDepths
	 * @return InspectedTypeInterface
	 */
	public function doInspectRecursiveArray(array $variable, array &$known, int $untilDepths) : InspectedTypeInterface
	{
		/** @phpstan-ignore-next-line */
		$known[] = $variable;
		$types = [];
		
		if(0 < $untilDepths)
		{
			foreach($variable as $children)
			{
				$alreadyKnown = false;
				
				foreach($known as $knownObject)
				{
					if($children === $knownObject)
					{
						$alreadyKnown = true;
						break;
					}
				}
				
				if($alreadyKnown)
				{
					continue;
				}
				
				/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
				$type = $this->doInspectRecursive($children, $known, $untilDepths - 1);
				$alreadyFound = false;
				
				foreach($types as $knownTypes)
				{
					if($type->equals($knownTypes))
					{
						$alreadyFound = true;
						break;
					}
				}
				
				if($alreadyFound)
				{
					continue;
				}
				
				$types[] = $type;
			}
		}
		
		/** @psalm-suppress ReferenceConstraintViolation */
		return new ArrayInspectedType($types);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectorInterface::inspect()
	 */
	public function inspect($variable, int $untilDepths = 10) : InspectedTypeInterface
	{
		$known = [];
		
		return $this->doInspectRecursive($variable, $known, $untilDepths);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
}
