<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * ArrayInspectedType class file.
 *
 * This class represents the fact that an array was inspected.
 *
 * @author Anastaszor
 */
class ArrayInspectedType implements InspectedTypeInterface
{
	
	/**
	 * All the inner types of this array.
	 *
	 * @var array<integer, InspectedTypeInterface>
	 */
	protected array $_innerTypes = [];
	
	/**
	 * Builds a new ArrayInspectedType with the given inner types.
	 *
	 * @param array<integer, InspectedTypeInterface> $innertypes
	 */
	public function __construct(array $innertypes)
	{
		$this->_innerTypes = $innertypes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		if(0 === \count($this->_innerTypes))
		{
			return '[...]';
		}
		
		$res = [];
		
		foreach($this->_innerTypes as $innertype)
		{
			$res[] = $innertype->__toString();
		}
		
		return '['.\implode(', ', $res).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		if(!$object instanceof self)
		{
			return false;
		}
		
		if(\count($this->_innerTypes) !== \count($object->_innerTypes))
		{
			return false;
		}
		
		foreach($this->_innerTypes as $ownInnerType)
		{
			$thisIsKnown = false;
			
			foreach($object->_innerTypes as $otherInnerType)
			{
				if($ownInnerType->equals($otherInnerType))
				{
					$thisIsKnown = true;
					break;
				}
			}
			if(!$thisIsKnown)
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		$smp = [];
		
		foreach($this->_innerTypes as $innertype)
		{
			$smp[] = $innertype->getSample();
		}
		
		return '['.\implode(', ', $smp).']';
	}
	
}
