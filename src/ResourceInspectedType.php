<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * ResourceInspectedType class file.
 *
 * This class represents the fact that a resource was inspected.
 *
 * @author Anastaszor
 */
class ResourceInspectedType implements InspectedTypeInterface
{
	
	/**
	 * The type of resource that was inspected.
	 *
	 * @var resource
	 */
	protected $_resource;
	
	/**
	 * Builds a new ResourceInspectedType with the given resource.
	 *
	 * @param resource $resource
	 */
	public function __construct($resource)
	{
		$this->_resource = $resource;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'resource('.$this->getType().')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self 
			&& $this->getType() === $object->getType();
	}
	
	/**
	 * Gets the type of resource.
	 *
	 * @return string
	 */
	public function getType() : string
	{
		return \get_resource_type($this->_resource);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		return $this->__toString();
	}
	
}
