<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * StringInspectedType class file.
 *
 * This class represents the fact that a string was inspected.
 *
 * @author Anastaszor
 */
class StringInspectedType implements InspectedTypeInterface
{
	
	/**
	 * The string value.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new StringInspectedType with the given string value.
	 *
	 * @param string $value
	 */
	public function __construct(string $value)
	{
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'string';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		if(30 < \mb_strlen($this->_value))
		{
			return ((string) \mb_substr($this->_value, 0, 27)).'...';
		}
		
		return $this->_value;
	}
	
}
