<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * IntegerInspectedType class file.
 *
 * This class represents the fact that an integer was inspected.
 *
 * @author Anastaszor
 */
class IntegerInspectedType implements InspectedTypeInterface
{
	
	/**
	 * The integer value.
	 *
	 * @var integer
	 */
	protected int $_value;
	
	/**
	 * Builds a new IntegerInspectedType with the given integer value.
	 *
	 * @param integer $value
	 */
	public function __construct(int $value)
	{
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'integer';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		return (string) $this->_value;
	}
	
}
