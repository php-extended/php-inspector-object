<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * FloatInspectedType class file.
 *
 * This class represents the fact that a float/double was inspected.
 *
 * @author Anastaszor
 */
class FloatInspectedType implements InspectedTypeInterface
{
	
	/**
	 * The float value.
	 * 
	 * @var float
	 */
	protected float $_value;
	
	/**
	 * Builds a new FloatInspectedType with the given float value.
	 * 
	 * @param float $value
	 */
	public function __construct(float $value)
	{
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'float';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		return (string) $this->_value; 
	}
	
}
