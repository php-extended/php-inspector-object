<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

/**
 * NullInspectedType class file.
 *
 * This class represents the fact that a null pointer was inspected.
 *
 * @author Anastaszor
 */
class NullInspectedType implements InspectedTypeInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'null';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Inspector\InspectedTypeInterface::getSample()
	 */
	public function getSample() : string
	{
		return 'null';
	}
	
}
